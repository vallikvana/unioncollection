//
//  HomeViewController.swift
//  UnionCollection
//
//  Created by kvanaMini1 on 15/07/16.
//  Copyright © 2016 kvanaMini1. All rights reserved.
//

import UIKit

class OverViewViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate {
    
 
    @IBAction func closeBtn(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
      
    }
    @IBAction func moreBtn(sender: AnyObject) {
    }
    @IBOutlet weak var userImageView: UIImageView!
 
    @IBOutlet weak var collectionView: UICollectionView!
    var ImageArray = ["profile","overview","lists","group","timeline","settings"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
       collectionView.delegate=self
        collectionView.dataSource=self
        self.userImageView.round(UIColor.whiteColor(), borderWidth: 2.0)
        
        self.navigationController?.navigationBarHidden = true
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ImageArray.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell : CollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("menuIdentifier", forIndexPath: indexPath) as! CollectionViewCell
                
        cell.imageView.image = UIImage(named: ImageArray[indexPath.row])
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        return
    }
    



}
