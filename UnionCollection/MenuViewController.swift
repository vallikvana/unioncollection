//
//  MenuViewController.swift
//  UnionCollection
//
//  Created by Narendra Kumar on 7/20/16.
//  Copyright © 2016 kvanaMini1. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,textMessageProtocol {

    @IBOutlet weak var tableView: UITableView!
    
    var outPutMessage: String!
    var messageArray : NSMutableArray = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       self.view.backgroundColor = UIColor(patternImage: UIImage(named: "bg_image3.png")!)
       
        
      }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func didSelectSave(data: String) {
        
       outPutMessage = data as String
     messageArray.insertObject(data, atIndex:0)
        self.tableView.reloadData()
        
    }
    

    @IBAction func leftSideButtonTapped(sender: AnyObject) {
    var appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    appDelegate.centerContainer!.toggleDrawerSide(MMDrawerSide.Left, animated: true, completion: nil)
    }


    @IBAction func addButtonTapped(sender: AnyObject) {
    }
    
    
    @IBAction func logoutBtn(sender: AnyObject) {
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messageArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("menuViewIdentifier")as! MenuViewCell
       
        cell.dataLabel.text = messageArray[indexPath.row] as? String
        
        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let vc: DataViewController = segue.destinationViewController as! DataViewController
        
        vc.delegate = self
    }
}
