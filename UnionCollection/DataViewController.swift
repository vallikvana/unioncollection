//
//  DataViewController.swift
//  UnionCollection
//
//  Created by Narendra Kumar on 7/21/16.
//  Copyright © 2016 kvanaMini1. All rights reserved.
//

import UIKit

protocol textMessageProtocol {
    func didSelectSave(data: String)
}

class DataViewController: UIViewController {
    
    var delegate: textMessageProtocol?

    @IBAction func saveBtn(sender: AnyObject) {
       self.delegate!.didSelectSave(dataTextView.text)
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBOutlet weak var dataTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
 

}
