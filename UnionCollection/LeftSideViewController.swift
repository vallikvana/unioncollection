//
//  LeftSideViewController.swift
//  UnionCollection
//
//  Created by Narendra Kumar on 7/20/16.
//  Copyright © 2016 kvanaMini1. All rights reserved.
//

import UIKit

extension UIView {
    func round(color : UIColor, borderWidth : CGFloat){
        self.layer.cornerRadius = self.frame.size.width / 2
        self.clipsToBounds = true
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = color.CGColor
    }
}

class LeftSideViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBOutlet weak var profilename: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    var menuDetails = ["Home","Overview","Lists","Groups","Settings"]
    var menuImageDetails = ["home_Icon","overview_Icon","lists_Icon","groups_Icon","settings_Icon"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "bg_image2.png")!)
        self.profileImage.round(UIColor.whiteColor(), borderWidth: 2.0)
        
        tableView.delegate=self
        tableView.dataSource=self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuDetails.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("menuDetailIdentifier")as! CustomViewCell
        cell.menuImages.image = UIImage(named: menuImageDetails[indexPath.row])
        cell.menuLabel.text = menuDetails[indexPath.row]
        cell.backgroundColor = UIColor(patternImage: UIImage(named: "bg_image3.png")!)
        tableView.backgroundColor = UIColor(patternImage: UIImage(named: "bg_image3.png")!)
        
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        switch(indexPath.row)
        {
            
        case 0:
            
            let viewController = self.storyboard?.instantiateViewControllerWithIdentifier("MenuViewController") as! MenuViewController
            
            let menuNavController = UINavigationController(rootViewController: viewController)
            
            let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            
            appDelegate.centerContainer!.centerViewController = menuNavController
            appDelegate.centerContainer!.toggleDrawerSide(MMDrawerSide.Left, animated: true, completion: nil)
            
            break;
            
        case 1:
            
            let homeViewController = self.storyboard?.instantiateViewControllerWithIdentifier("OverViewViewController") as! OverViewViewController
            
            let homeNavController = UINavigationController(rootViewController: homeViewController)
            
            let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            
            appDelegate.centerContainer!.centerViewController = homeNavController
            appDelegate.centerContainer!.toggleDrawerSide(MMDrawerSide.Left, animated: true, completion: nil)
            
            break;
            
        case 2:
            let listsViewController = self.storyboard?.instantiateViewControllerWithIdentifier("ListsViewController") as! ListsViewController
            
            let listsNavController = UINavigationController(rootViewController: listsViewController)
            
            let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            
            appDelegate.centerContainer!.centerViewController = listsNavController
            appDelegate.centerContainer!.toggleDrawerSide(MMDrawerSide.Left, animated: true, completion: nil)
            
            
        default:
            
            print("\(menuDetails[indexPath.row]) is selected")
            
        }
        
    }
}
